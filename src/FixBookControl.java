public class FixBookControl {
	
	private FixBookUI ui;
	private enum BOOKCONTROL_STATE { INITIALISED, READY, FIXING };
	private BOOKCONTROL_STATE state;
	
	private library library;
	private book currentBook;


	public FixBookControl() {
		this.library = library.INSTANCE();
		state = BOOKCONTROL_STATE.INITIALISED;
	}
	

        
	public void setUI(FixBookUI ui) {
		if (!state.equals(BOOKCONTROL_STATE.INITIALISED)) {
			throw new RuntimeException("FixBookControl: cannot call setUI except in INITIALISED state");
		}	
		this.ui = ui;
		ui.setState(FixBookUI.UI_STATE.READY);
		state = BOOKCONTROL_STATE.READY;		
	}


	public void bookScanned(int bookId) {
		if (!state.equals(BOOKCONTROL_STATE.READY)) {
			throw new RuntimeException("FixBookControl: cannot call bookScanned except in READY state");
		}	
		currentBook = library.Book(bookId);
		
		if (currentBook == null) {
			ui.display("Invalid bookId");
			return;
		}
		if (!currentBook.Damaged()) {
			ui.display("\"Book has not been damaged");
			return;
		}
		ui.display(currentBook.toString());
		ui.setState(FixBookUI.UI_STATE.FIXING);
		state = BOOKCONTROL_STATE.FIXING;		
	}


	public void Fixbook(boolean fix) {
		if (!state.equals(BOOKCONTROL_STATE.FIXING)) {
			throw new RuntimeException("FixBookControl: cannot call fixBook except in FIXING state");
		}	
		if (fix) {
			library.repairBook(currentBook);
		}
		currentBook = null;
		ui.setState(FixBookUI.UI_STATE.READY);
		state = BOOKCONTROL_STATE.READY;		
	}

	
	public void scanningComplete() {
		if (!state.equals(BOOKCONTROL_STATE.READY)) {
			throw new RuntimeException("FixBookControl: cannot call scanningComplete except in READY state");
		}	
		ui.setState(FixBookUI.UI_STATE.COMPLETED);		
	}






}
