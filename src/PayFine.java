public class PayFine {
	
	private PayFineUI ui;
	private enum FINECONTROL_STATE { INITIALISED, READY, PAYING, COMPLETED, CANCELLED };
	private FINECONTROL_STATE state;
	
	private library library;
	private member member;


	public PayFine() {
		this.library = library.INSTANCE();
		state = FINECONTROL_STATE.INITIALISED;
	}
	
	
	public void setUI(PayFineUI ui) {
		if (!state.equals(FINECONTROL_STATE.INITIALISED)) {
			throw new RuntimeException("PayFineControl: cannot call setUI except in INITIALISED state");
		}	
		this.ui = ui;
		ui.setState(PayFineUI.UI_STATE.READY);
		state = FINECONTROL_STATE.READY;		
	}


	public void MemberInfo(int memberId) {
		if (!state.equals(FINECONTROL_STATE.READY)) {
			throw new RuntimeException("PayFineControl: cannot call cardSwiped except in READY state");
		}	
		member = library.getMember(memberId);
		
		if (member == null) {
			ui.display("Invalid Member Id");
			return;
		}
		ui.display(member.toString());
		ui.setState(PayFineUI.UI_STATE.PAYING);
		state = FINECONTROL_STATE.PAYING;
	}
	
	
	public void cancel() {
		ui.setState(PayFineUI.UI_STATE.CANCELLED);
		state = FINECONTROL_STATE.CANCELLED;
	}


	public double payFine(double amount) {
		if (!state.equals(FINECONTROL_STATE.PAYING)) {
			throw new RuntimeException("PayFineControl: cannot call payFine except in PAYING state");
		}	
		double change = member.payFine(amount);
		if (change > 0) {
			ui.display(String.format("Change: $%.2f", change));
		}
		ui.display(member.toString());
		ui.setState(PayFineUI.UI_STATE.COMPLETED);
		state = FINECONTROL_STATE.COMPLETED;
		return change;
	}
	


}
